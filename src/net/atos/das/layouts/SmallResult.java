package net.atos.das.layouts;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import net.atos.das.R;
import net.atos.das.activities.ViewPersonActivity;
import net.atos.das.person.Person;

/**
 * Created by a543097 on 07/10/2014.
 */
public class SmallResult extends LinearLayout {

    Person person;

    public SmallResult(Context context) {
        super(context);
        LayoutInflater.from(getContext()).inflate(R.layout.small_result,null);
    }


    public SmallResult(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public SmallResult(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPerson(Person p){
        this.person = p;

        TextView name = (TextView)findViewById(R.id.small_name);
        ImageView picture = (ImageView)findViewById(R.id.small_image);

        name.setText(person.name);
        picture.setImageBitmap(person.photo.bitmap);
    }

    public Person getPerson(){
        return person;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SmallResult sr = (SmallResult)v;

                Intent i = new Intent(getContext(), ViewPersonActivity.class).putExtra("person", (android.os.Parcelable) sr.person);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(i);
            }
        });


    }
}
