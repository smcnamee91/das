package net.atos.das.person;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;
import android.widget.TextView;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.util.Base64;
import net.atos.das.R;
import net.atos.das.hack.SerializableBitmap;
import net.atos.das.utils.ConnectionManager;

import java.io.Serializable;

/**
 * Created by a543097 on 07/10/2014.
 */
public class Person implements Parcelable, Serializable{

    public String name;
    public String empID;
    public SerializableBitmap photo;
    public String phoneNumber;
    public String emailAddress;
    public String location;
    public String locationDN;

    public SearchResultEntry locationResult;

    private static SerializableBitmap defaultImage = null;

    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[0];
        }
    };

    private Person(Parcel in){
        name = in.readString();
        empID = in.readString();
        photo = (SerializableBitmap)in.readSerializable();
        emailAddress = in.readString();
        phoneNumber = in.readString();
        location = in.readString();
        locationDN = in.readString();
    }

    public Person(SearchResultEntry person, Resources res){

        if(defaultImage == null){
            defaultImage = new SerializableBitmap(R.drawable.default_photo);
        }
        name = person.getAttributeValue("displayname");
        empID = person.getAttributeValue("employeenumber");
        try {
            photo = new SerializableBitmap(person.getAttribute("jpegPhoto"));
        }catch(NullPointerException e){
            photo = defaultImage;
        }
        phoneNumber = person.getAttributeValue("mobile");
        emailAddress = person.getAttributeValue("mail");
        locationDN = person.getAttributeValue("aosite1");

        new LocationResolve().execute();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(empID);
        dest.writeSerializable(photo);
        dest.writeString(emailAddress);
        dest.writeString(phoneNumber);
        dest.writeString(location);
        dest.writeString(locationDN);
    }

    private class LocationResolve extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
           locationResult = ConnectionManager.resolveLocation(locationDN);
           location = "" + locationResult.getAttributeValue("name") + ", "
                    + locationResult.getAttributeValue("l") + ", "
                    + locationResult.getAttributeValue("st") + ", "
                    + locationResult.getAttributeValue("postalcode");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
