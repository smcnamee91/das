package net.atos.das.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.unboundid.ldap.sdk.SearchResultEntry;
import net.atos.das.R;
import net.atos.das.activities.MainActivity;
import net.atos.das.layouts.SmallResult;
import net.atos.das.person.Person;
import net.atos.das.utils.ConnectionManager;
import net.atos.das.utils.PreviousSearch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a543097 on 14/10/2014.
 */
public class PreviousSearchFragment extends Fragment {

    protected static View root;
    protected static Context context;
    private ArrayList<PreviousSearch> searches;

    LinearLayout searchContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.previous_search_layout,container,false);
        this.searches = MainActivity.previousSearches;
        root = rootView;
        context = root.getContext();

        searchContainer = (LinearLayout)root.findViewById(R.id.previous_search_container);

        for(PreviousSearch p : searches){
            final PreviousSearch per = p;
            LinearLayout ll = (LinearLayout)inflater.inflate(R.layout.previous_search_list_item,searchContainer,false);
            TextView term = (TextView)ll.findViewById(R.id.text_search_term);
            TextView count = (TextView)ll.findViewById(R.id.text_result_count);

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager manager = getFragmentManager();
                    Bundle args = new Bundle();
                    args.putSerializable("Search",per);
                    ShowResultsFragment srf = new ShowResultsFragment();
                    srf.setArguments(args);

                    manager.beginTransaction().replace(R.id.content_frame,srf).addToBackStack("searchy?").commit();

                    getActivity().getActionBar().setTitle(per.getTerm());
                }
            });

            term.setText(p.getTerm());
            count.setText("" + p.getResultCount() + " results");

            searchContainer.addView(ll);
        }

        return rootView;
    }


}