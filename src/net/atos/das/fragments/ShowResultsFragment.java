package net.atos.das.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import net.atos.das.R;
import net.atos.das.layouts.SmallResult;
import net.atos.das.person.Person;
import net.atos.das.utils.PreviousSearch;

import java.util.ArrayList;

/**
 * Created by a543097 on 15/10/2014.
 */
public class ShowResultsFragment extends Fragment {

    PreviousSearch prev;
    ArrayList<Person> results;

    public ShowResultsFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.show_results_fragment, container, false);

        prev = (PreviousSearch)getArguments().getSerializable("Search");
        results = prev.getResults();

        LinearLayout ll = (LinearLayout)root.findViewById(R.id.multiple_results);

        for(Person p : results){
            SmallResult sr = (SmallResult)LayoutInflater.from(root.getContext().getApplicationContext()).inflate(R.layout.small_result,null);
            sr.setPerson(p);

            ll.addView(sr);
        }

        return root;

    }


}