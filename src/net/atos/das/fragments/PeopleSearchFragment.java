package net.atos.das.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.unboundid.ldap.sdk.SearchResultEntry;
import net.atos.das.R;
import net.atos.das.activities.MainActivity;
import net.atos.das.layouts.SmallResult;
import net.atos.das.person.Person;
import net.atos.das.utils.ConnectionManager;
import net.atos.das.utils.PreviousSearch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a543097 on 14/10/2014.
 */
public class PeopleSearchFragment extends Fragment {

    protected static View root;
    protected static Context context;
    private ArrayList<Person> results;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.people_search_layout,container,false);

        root = rootView;
        context = root.getContext();

        root.findViewById(R.id.search_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SearchTask().execute();

                EditText myEditText = (EditText) root.findViewById(R.id.search_box);
                myEditText.clearFocus();
                InputMethodManager imm = (InputMethodManager)context.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);

            }
        });

        return rootView;
    }

    private class SearchTask extends AsyncTask<Void, Void, List<SearchResultEntry>> {

        String name = "";
        boolean multiple = false;

        Person person;
        List<SearchResultEntry> people;

        ProgressDialog pd = new ProgressDialog(context);

        @Override
        protected void onPreExecute() {
            EditText t = (EditText)root.findViewById(R.id.search_box);
            name = t.getText().toString();

            pd.setMessage("Searching");
            pd.setCanceledOnTouchOutside(false);
            pd.show();

        }

        @Override
        protected List<SearchResultEntry> doInBackground(Void... params) {
            try {
                people = ConnectionManager.findPerson(name);
            }catch (NullPointerException e){
                cancel(true);
            }

            return people;
        }

        @Override
        protected void onPostExecute(List<SearchResultEntry> param) {

            if(param.size() > 0){
                LinearLayout ll = (LinearLayout)root.findViewById(R.id.person_multiple_results);
                ll.removeAllViews();
                results = new ArrayList<Person>();
                for(SearchResultEntry entry : param){
                    Person p = new Person(entry,getResources());
                    results.add(p);
                    SmallResult sr = (SmallResult)LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.small_result,null);
                    sr.setPerson(p);
                    ll.addView(sr);
                }
                MainActivity.previousSearches.add(new PreviousSearch(results,name));
            }else{
                pd.dismiss();
                cancel(true);
            }
            pd.dismiss();
            if(param.size() > 1){
                Toast.makeText(context, param.size() + " results found", Toast.LENGTH_LONG).show();
            }
        }

       @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(context,"No results found", Toast.LENGTH_LONG).show();
        }
    }
}