package net.atos.das.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import net.atos.das.R;

/**
 * Created by a543097 on 14/10/2014.
 */
public class PlacesSearchFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.places_search_layout,container,false);

        return rootView;
    }

//    private class SearchTask extends AsyncTask<Void, Void, List<SearchResultEntry>> {
//
//        String name = "";
//        boolean multiple = false;
//
//        Person person;
//        List<SearchResultEntry> people;
//
//        ProgressDialog pd = new ProgressDialog(MainActivity.context);
//
//        @Override
//        protected void onPreExecute() {
//            EditText t = (EditText)findViewById(R.id.search_box);
//            name = t.getText().toString();
//
//            pd.setMessage("Searching");
//
//            pd.show();
//
//        }
//
//        @Override
//        protected List<SearchResultEntry> doInBackground(Void... params) {
//            try {
//                people = ConnectionManager.findPerson(name);
//            }catch (NullPointerException e){
//                cancel(true);
//            }
//
//            return people;
//        }
//
//        @Override
//        protected void onPostExecute(List<SearchResultEntry> param) {
//
//
//
//            hideBigView();
//
//            if(param.size() == 1){
//                showBigView();
//
//                TextView name = (TextView)findViewById(R.id.name_field);
//                TextView empID = (TextView)findViewById(R.id.emp_id_field);
//                ImageView photo = (ImageView)findViewById(R.id.image_box);
//                TextView email = (TextView)findViewById(R.id.email_field);
//                TextView mobile = (TextView)findViewById(R.id.mobile_field);
//
//                Person person = new Person(param.get(0),getResources());
//                name.setText(person.name);
//                empID.setText(person.empID);
//                photo.setImageBitmap(person.photo);
//                email.setText(person.emailAddress);
//                mobile.setText(person.phoneNumber);
//            }else if(param.size() > 1){
//                hideBigView();
//                LinearLayout ll = (LinearLayout)findViewById(R.id.multiple_results);
//                ll.removeAllViews();
//
//                for(SearchResultEntry entry : param){
//                    Person p = new Person(entry,getResources());
//                    SmallResult sr = (SmallResult)LayoutInflater.from(getApplicationContext()).inflate(R.layout.small_result,null);
//                    sr.setPerson(p);
//                    ll.addView(sr);
//                }
//            }else{
//                pd.dismiss();
//                cancel(true);
//            }
//            pd.dismiss();
//            if(param.size() > 1){
//                Toast.makeText(context, param.size() + " results found", Toast.LENGTH_LONG).show();
//            }
//        }
//
//        private void hideBigView(){
//            LinearLayout ll = (LinearLayout)findViewById(R.id.multiple_results);
//            ll.removeAllViews();
//            ll.setVisibility(LinearLayout.VISIBLE);
//            LinearLayout bigView = (LinearLayout)findViewById(R.id.big_container);
//            bigView.setVisibility(LinearLayout.GONE);
//        }
//
//        private void showBigView(){
//            LinearLayout ll = (LinearLayout)findViewById(R.id.multiple_results);
//            ll.removeAllViews();
//            ll.setVisibility(LinearLayout.GONE);
//            LinearLayout bigView = (LinearLayout)findViewById(R.id.big_container);
//            bigView.setVisibility(LinearLayout.VISIBLE);
//        }
//
//        @Override
//        protected void onCancelled() {
//            super.onCancelled();
//            Toast.makeText(getApplicationContext(),"No results found", Toast.LENGTH_LONG).show();
//        }
//    }
}