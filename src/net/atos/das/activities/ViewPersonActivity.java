package net.atos.das.activities;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import net.atos.das.R;
import net.atos.das.person.Person;
import net.atos.das.utils.ConnectionManager;

/**
 * Created by a543097 on 08/10/2014.
 */
public class ViewPersonActivity extends Activity {

    Person person;

    public ViewPersonActivity(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_person_activity);

        this.person = (Person)getIntent().getExtras().get("person");

        TextView name = (TextView)findViewById(R.id.large_name);
        TextView empID = (TextView)findViewById(R.id.large_emp_id);
        ImageView photo = (ImageView)findViewById(R.id.large_photo);
        TextView email = (TextView)findViewById(R.id.large_email);
        TextView mobile = (TextView)findViewById(R.id.large_mobile);


        name.setText(person.name);
        empID.setText(person.empID);
        photo.setImageBitmap(person.photo.bitmap);
        email.setText(person.emailAddress);
        mobile.setText(person.phoneNumber);


        if (person.location == null) {
//            new LocationResolve().execute();
        }else {
            TextView location = (TextView)findViewById(R.id.large_location);
            location.setText(person.location);
        }


    }



}