package net.atos.das.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.unboundid.asn1.ASN1OctetString;
import com.unboundid.asn1.ASN1StreamReader;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.util.Base64;
import com.unboundid.util.ByteStringBuffer;
import net.atos.das.R;
import net.atos.das.utils.ConnectionManager;

import java.text.AttributedCharacterIterator;
import java.text.ParseException;

public class LoginActivity extends Activity {
    /**
     * Called when the activity is first created.
     */


    public static boolean ok;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button loginButton = (Button)findViewById(R.id.loginbutton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetSessionTask().execute();
            }
        });

    }

    private class GetSessionTask extends AsyncTask<Void, Void, Boolean>{

        String user = "";
        byte[] pass;

        @Override
        protected void onPreExecute(){
            Button loginButton = (Button)findViewById(R.id.loginbutton);
            loginButton.setEnabled(false);

            EditText usernameText = (EditText)findViewById(R.id.txtusername);
            user = usernameText.getText().toString();
            EditText passwordText = (EditText)findViewById(R.id.txtpassword);
            pass = passwordText.getText().toString().getBytes();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            boolean connected = ConnectionManager.authenticateUser(user, pass);

            return connected;
        }


        protected void onPostExecute(Boolean connected){
            if(connected) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            } else{
                Button loginButton = (Button)findViewById(R.id.loginbutton);
                loginButton.setEnabled(true);
                Toast.makeText(getApplicationContext(),"Login failed! Check username and password",Toast.LENGTH_LONG).show();
            }
        }
    }
}
