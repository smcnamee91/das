package net.atos.das.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import net.atos.das.R;
import net.atos.das.fragments.PeopleSearchFragment;
import net.atos.das.fragments.PlacesSearchFragment;
import net.atos.das.fragments.PreviousSearchFragment;
import net.atos.das.fragments.ShowResultsFragment;
import net.atos.das.layouts.SmallResult;
import net.atos.das.person.Person;
import net.atos.das.utils.ConnectionManager;
import net.atos.das.utils.PreviousSearch;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by a543097 on 06/10/2014.
 */
public class MainActivity extends Activity {

    public static Context context;
    private String[] options = new String[]{"People","Places","Previous Searches"};

    DrawerLayout drawerLayout;
    ListView drawerList;
    ActionBarDrawerToggle drawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    Toast exitToast;

    public static ArrayList<PreviousSearch> previousSearches = new ArrayList<PreviousSearch>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        this.context = this;

        mTitle = mDrawerTitle = getTitle();

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerList = (ListView)findViewById(R.id.left_drawer);

        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        drawerList.setAdapter(new ArrayAdapter<String>(this,R.layout.drawer_list_item,options));

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        if (savedInstanceState == null) {
            selectItem(0);
        }

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment fragment = getFragmentManager().findFragmentById(R.id.content_frame);
                setTitleFromFragment(fragment);
            }



            private void setTitleFromFragment(Fragment fragment) {
                if (fragment instanceof PeopleSearchFragment) {
                    getActionBar().setTitle("People");
                } else if (fragment instanceof PlacesSearchFragment) {
                    getActionBar().setTitle("Places");
                } else if (fragment instanceof PreviousSearchFragment) {
                    getActionBar().setTitle("Previous Searches");
                } else {

                }
            }
        });

        exitToast = Toast.makeText(getApplicationContext(),"Press back again to exit",Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackPressed(){
        if(exitToast.getView().isShown()){
            super.onBackPressed();
        }else if(getFragmentManager().getBackStackEntryCount() == 0){

           exitToast.show();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(drawerList);
        return super.onPrepareOptionsMenu(menu);
    }

    private void selectItem(int position){

        FragmentManager manager = getFragmentManager();

        switch(position){
            case 0:
                if(getFragmentManager().getBackStackEntryCount() != 0) {
                    manager.beginTransaction().replace(R.id.content_frame, new PeopleSearchFragment()).addToBackStack("people").commit();
                }else{
                    // First run will end up here, this stops the fragment just disappearing
                    manager.beginTransaction().replace(R.id.content_frame, new PeopleSearchFragment()).commit();
                }
                break;
            case 1:
                manager.beginTransaction().replace(R.id.content_frame,new PlacesSearchFragment()).addToBackStack("places").commit();
                break;
            case 2:
                manager.beginTransaction().replace(R.id.content_frame,new PreviousSearchFragment()).addToBackStack("previous").commit();
                break;
            default:
                manager.beginTransaction().replace(R.id.content_frame,new PeopleSearchFragment()).commit();
                break;
        }

        // update selected item and title, then close the drawer
        drawerList.setItemChecked(position, true);
        setTitle(options[position]);
        drawerLayout.closeDrawer(drawerList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

}