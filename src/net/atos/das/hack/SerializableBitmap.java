package net.atos.das.hack;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.util.Base64;
import net.atos.das.R;
import net.atos.das.activities.MainActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by a543097 on 15/10/2014.
 */
public class SerializableBitmap implements Serializable {

    public Bitmap bitmap;

    public SerializableBitmap(int resource){
        this.bitmap = BitmapFactory.decodeResource(MainActivity.context.getResources(),resource);
    }

    public SerializableBitmap(Attribute photoAttribute) {
        String base64 = photoAttribute.toString();

        int start = (base64.indexOf("base64Values={'") + 15);

        int end = (base64.indexOf("'})") + 0);

        base64 = base64.substring(start,end);

        byte[] decodedString = new byte[0];

        try {
            decodedString = Base64.decode(base64);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inSampleSize = 2;

        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, bounds);

       this.bitmap = decodedByte;
    }

    // Converts the Bitmap into a byte array for serialization
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteStream);
        byte bitmapBytes[] = byteStream.toByteArray();
        out.write(bitmapBytes, 0, bitmapBytes.length);
    }

    // Deserializes a byte array representing the Bitmap and decodes it
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int b;

        while((b = in.read()) != -1){
            byteStream.write(b);
        }

        byte bitmapBytes[] = byteStream.toByteArray();
        bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
    }
}
