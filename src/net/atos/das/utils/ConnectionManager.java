package net.atos.das.utils;

/**
 * Created by a543097 on 06/10/2014.
 */

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import com.unboundid.asn1.ASN1OctetString;
import com.unboundid.ldap.sdk.*;
import com.unboundid.ldap.sdk.controls.*;
import com.unboundid.util.ssl.HostNameTrustManager;
import com.unboundid.util.ssl.SSLUtil;
import com.unboundid.util.ssl.TrustAllTrustManager;
import net.atos.das.activities.MainActivity;
import net.atos.das.layouts.SmallResult;

import javax.net.ssl.SSLSocketFactory;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class ConnectionManager {

    private static Timer t = new Timer();

    private static final String ldapServer = "ldap.myatos.net";
    private static final int ldapPort = 636;

    private static BindRequest bindRequest;

    private static final String baseDN = "ou=people,dc=atosorigin,dc=com";

    private static LDAPConnection connection;

    public static boolean authenticateUser(String username, byte[] password){

        boolean result = false;
        try {
            SSLUtil sslUtil = new SSLUtil(new HostNameTrustManager(false,ldapServer));
            SSLSocketFactory sslSocketFactory = sslUtil.createSSLSocketFactory();

            LDAPConnection connection = new LDAPConnection(sslSocketFactory);
            connection.connect(ldapServer, ldapPort);

            bindRequest = new SimpleBindRequest(createUserDN(username),password);
            BindResult bindResult = connection.bind(bindRequest);

            result = connection.isConnected();

            setLdapConnection(connection);

        } catch (Exception e) {
            result = false;
            e.printStackTrace();
        }

        return result;
    }

    private static String createUserDN(String username){
        String result = "";

        if(username.startsWith("a")||username.startsWith("A")){
            result = "aoLdapKey=aa" + username + ",ou=people,dc=atosorigin,dc=com";
        }else if(username.startsWith("s")||username.startsWith("S")){
            result = "aoLdapKey=xx" + username + ",ou=people,dc=atosorigin,dc=com";
        }else{
            // Invalid username
        }

        return result;
    }

    public static List<SearchResultEntry> findPerson(String name){
        if(!connection.isConnected()){
            try {
                connection.connect(ldapServer,ldapPort);
                connection.bind(bindRequest);
            } catch (LDAPException e) {
                e.printStackTrace();
            }
        }

        List<SearchResultEntry> result = null;
        SearchResultEntry match = null;

        if(name.trim().split(" ").length > 1){
            result = searchFullName(name);
        }else if(name.trim().split(" ").length == 1){
            result = searchFirstOrLastName(name);
        }

        return result;
    }

    private static List<SearchResultEntry> searchFirstOrLastName(String name){
        PowerManager mgr = (PowerManager)MainActivity.context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = mgr.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "MyWakeLock");
        wakeLock.acquire();

        if(!connection.isConnected()){
            try {
                connection.connect(ldapServer,ldapPort);
                connection.bind(bindRequest);
            } catch (LDAPException e) {
                e.printStackTrace();
            }
        }

        SRL sl = new SRL();

        ASN1OctetString resumeCookie = null;
        boolean moreResults = true;

        int offset = 1;
        int count = 0;
        SearchRequest request = new SearchRequest(
                sl,
                baseDN,
                SearchScope.ONE,
                Filter.createORFilter(Filter.createEqualityFilter("givenname", name),
                        Filter.createEqualityFilter("sn", name)),
                        "displayname","employeenumber","jpegphoto","mobile","mail","aosite1");
//        request.setControls(new ServerSideSortRequestControl(new SortKey("sn"),
//                new SortKey("givenName")),new SimplePagedResultsControl(1,resumeCookie));
        request.setControls(new ServerSideSortRequestControl(new SortKey("sn"),
                new SortKey("givenName")),new VirtualListViewRequestControl(offset,0, 9, count, resumeCookie));
        request.setFollowReferrals(false);
        VirtualListViewResponseControl paged = null;
        while(moreResults){

            try {

                request.setControls(new ServerSideSortRequestControl(new SortKey("sn"),
                        new SortKey("givenName")),new VirtualListViewRequestControl(offset,0, 9, count, resumeCookie));

                SearchResult pagedResult = connection.search(request);

                paged = VirtualListViewResponseControl.get(pagedResult);
                resumeCookie = paged.getContextID();
                offset += 10;
                count = paged.getContentCount();

                } catch (LDAPSearchException e) {
                    Log.e("dasError", e.getMessage());
                } catch (LDAPException le){
                    Log.e("dasError", le.getMessage());
                }

                if (offset > count)
                {
                    moreResults = false;
                }


        }
        Log.i("total", sl.getResults().size() + " entires returned");
        wakeLock.release();
        return sl.getResults();
    }

    private static List<SearchResultEntry> searchFullName(String name){
        List<SearchResultEntry> result = null;
        String firstName = "";
        String surname = "";

        if(!name.trim().isEmpty()){
            firstName = name.split(" ")[0];
            surname = name.split(" ")[1];
        }

        try {
            SearchRequest request = new SearchRequest(baseDN,SearchScope.ONE, Filter.createANDFilter(Filter.createEqualityFilter("givenname", firstName), Filter.createApproximateMatchFilter("sn", surname.getBytes())));
            request.setSizeLimit(100);
            result = connection.search(request).getSearchEntries();
        } catch (LDAPSearchException e) {
            e.printStackTrace();
        }

        return result;
    }

    private static void setLdapConnection(LDAPConnection con){
        connection = con;
    }

    public static boolean isAuthenticated(){
        return connection!=null;
    }

    public static SearchResultEntry resolveLocation(String locationDN){
        SearchResultEntry entry = null;

        if(!connection.isConnected()){
            try {
                connection.connect(ldapServer,ldapPort);
                connection.bind(bindRequest);
            } catch (LDAPException e) {
                e.printStackTrace();
            }
        }

        try {
           entry = connection.search("ou=sites,dc=atosorigin,dc=com", SearchScope.ONE, Filter.createEqualityFilter("uid",
                    locationDN.substring(
                            (locationDN.indexOf("uid=")+4),
                            locationDN.indexOf(","))),
                   "name","l","st","postalcode").getSearchEntries().get(0);
            return entry;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entry;
    }

    private static class SRL implements SearchResultListener{

        public List<SearchResultEntry> list = new ArrayList<SearchResultEntry>();

        @Override
        public void searchEntryReturned(SearchResultEntry searchResultEntry) {
            //Log.i("dasEntryReturned", searchResultEntry.getAttributeValue("displayname"));
            list.add(searchResultEntry);
        }

        @Override
        public void searchReferenceReturned(SearchResultReference searchResultReference) {

        }

        public List<SearchResultEntry> getResults(){
            return list;
        }
    }
}
