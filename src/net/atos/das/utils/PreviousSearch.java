package net.atos.das.utils;

import net.atos.das.person.Person;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by a543097 on 15/10/2014.
 */
public class PreviousSearch implements Serializable{

    ArrayList<Person> results;
    String searchTerm;

    public PreviousSearch(ArrayList<Person> results, String searchTerm){
        this.results = results;
        this.searchTerm = searchTerm;
    }

    public String getTerm(){
        return searchTerm;
    }

    public ArrayList<Person> getResults(){
        return results;
    }

    public int getResultCount(){
        return results.size();
    }
}
